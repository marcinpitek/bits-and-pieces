#pragma once

#include <boost/asio.hpp>

#include <iostream>
#include <memory>

class Handle {
public:
  explicit Handle(boost::asio::ip::tcp::socket&& socket) :
      socket{std::move(socket)} {}

  std::vector<uint8_t> read() {
    std::string data{};
    data.resize(4);
    boost::asio::read(socket, boost::asio::buffer(&data[0], 4));
    return std::vector<uint8_t>(data.begin(), data.end());
  }

  void write(std::vector<uint8_t>&& buff) {
    boost::system::error_code error_code;
    boost::asio::write(socket, boost::asio::buffer(buff),
                       boost::asio::transfer_all(), error_code);
  }

  void close() { socket.close(); }

private:
  boost::asio::ip::tcp::socket socket;
};
