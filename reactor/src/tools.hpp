#include <chrono>
#include <iostream>
#include <random>
#include <string_view>
#include <thread>

inline void delay() {
  std::mt19937_64 eng{std::random_device{}()};
  constexpr auto from{100};
  constexpr auto to{500};
  std::uniform_int_distribution<> dist{from, to};
  std::this_thread::sleep_for(std::chrono::milliseconds{dist(eng)});
}

inline void log_message(std::string_view msg, uint8_t msb, uint8_t lsb) {
  std::cout << msg << " " << static_cast<uint16_t>((msb << 8 | lsb)) // NOLINT
            << std::endl;
}
